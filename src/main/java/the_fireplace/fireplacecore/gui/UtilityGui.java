package the_fireplace.fireplacecore.gui;

import java.io.IOException;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
/**
 * 
 * @author The_Fireplace
 *
 */
@SideOnly(Side.CLIENT)
public class UtilityGui extends GuiScreen {
	private static final ResourceLocation modTabs = new ResourceLocation("textures/gui/container/creative_inventory/tabs.png");
	private static int selectedTabIndex = ModTabs.tabFireplaceCore.getTabIndex();
	private boolean wasClicking;
	private static int tabPage = 0;
	private int maxPages = 0;
	protected int guiLeft, guiTop;
	protected int xSize = 195;
	protected int ySize = 136;
	public UtilityGui(){
		ScaledResolution res = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
		this.allowUserInput = true;
		this.width = res.getScaledWidth();
		this.height = res.getScaledHeight();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
	}
	@Override
	public void initGui(){
		super.initGui();
		this.buttonList.clear();
		Keyboard.enableRepeatEvents(true);
		int i = selectedTabIndex;
		selectedTabIndex = -1;
		this.setCurrentModTab(ModTabs.tabArray[i]);
		int tabCount = ModTabs.tabArray.length;
		if (tabCount > 12)
		{
			buttonList.add(new GuiButton(101, guiLeft,              guiTop - 50, 20, 20, "<"));
			buttonList.add(new GuiButton(102, guiLeft + xSize - 20, guiTop - 50, 20, 20, ">"));
			maxPages = ((tabCount - 12) / 10) + 1;
		}
	}
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		super.keyTyped(typedChar, keyCode);
	}
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		if (mouseButton == 0)
		{
			int l = mouseX - this.guiLeft;
			int i1 = mouseY - this.guiTop;
			ModTabs[] amodtabs = ModTabs.tabArray;
			int j1 = amodtabs.length;
			for (int k1 = 0; k1 < j1; ++k1)
			{
				ModTabs modtabs = amodtabs[k1];
				if (this.switchToTab(modtabs, l, i1))
				{
					return;
				}
			}
		}
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	@Override
	protected void mouseReleased(int mouseX, int mouseY, int state)
	{
		if (state == 0)
		{
			int l = mouseX - this.guiLeft;
			int i1 = mouseY - this.guiTop;
			ModTabs[] amodtabs = ModTabs.tabArray;
			int j1 = amodtabs.length;
			for (int k1 = 0; k1 < j1; ++k1)
			{
				ModTabs modtabs = amodtabs[k1];
				if (modtabs != null && this.switchToTab(modtabs, l, i1))
				{
					this.setCurrentModTab(modtabs);
					return;
				}
			}
		}
		super.mouseReleased(mouseX, mouseY, state);
	}
	private void setCurrentModTab(ModTabs tab){
		if(tab == null)return;
		selectedTabIndex = tab.getTabIndex();
	}
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		int k = this.guiLeft;
		int l = this.guiTop;
		this.drawGuiContainerBackgroundLayer(partialTicks, mouseX, mouseY);
		GlStateManager.disableRescaleNormal();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableLighting();
		GlStateManager.disableDepth();
		boolean flag = Mouse.isButtonDown(0);//is the player clicking

		super.drawScreen(mouseX, mouseY, partialTicks);
		RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.pushMatrix();
		GlStateManager.translate(k, l, 0.0F);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.enableRescaleNormal();
		short short1 = 240;
		short short2 = 240;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, short1 / 1.0F, short2 / 1.0F);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		RenderHelper.disableStandardItemLighting();
		//RenderHelper.enableGUIStandardItemLighting();
		GlStateManager.popMatrix();
		GlStateManager.enableLighting();
		GlStateManager.enableDepth();
		RenderHelper.enableStandardItemLighting();
		ModTabs[] amodtabs = ModTabs.tabArray;
		int start = tabPage * 10;
		int i2 = Math.min(amodtabs.length, ((tabPage + 1) * 10) + 2);
		if (tabPage != 0) start += 2;
		boolean rendered = false;

		for (int j2 = start; j2 < i2; ++j2)
		{
			ModTabs modtabs = amodtabs[j2];
			if(modtabs == null)continue;
			if (this.renderModInventoryHoveringText(modtabs, mouseX, mouseY))
			{
				rendered = true;
				break;
			}
		}
		if (maxPages != 0)
		{
			String page = String.format("%d / %d", tabPage + 1, maxPages + 1);
			int width = fontRendererObj.getStringWidth(page);
			GlStateManager.disableLighting();
			this.zLevel = 300.0F;
			itemRender.zLevel = 300.0F;
			fontRendererObj.drawString(page, guiLeft + (xSize / 2) - (width / 2), guiTop - 44, -1);
			this.zLevel = 0.0F;
			itemRender.zLevel = 0.0F;
		}
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableLighting();
		GlStateManager.enableRescaleNormal();
		ModTabs.guiArray[selectedTabIndex].drawScreen(mouseX, mouseY, partialTicks);
	}
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		RenderHelper.enableGUIStandardItemLighting();
		ModTabs modtabs = ModTabs.tabArray[selectedTabIndex];
		ModTabs[] amodtabs = ModTabs.tabArray;
		int k = amodtabs.length;
		int start = tabPage * 10;
		k = Math.min(amodtabs.length, ((tabPage + 1) * 10 + 2));
		if (tabPage != 0) start += 2;
		for (int l = start; l < k; ++l)
		{
			ModTabs modtabs1 = amodtabs[l];
			this.mc.getTextureManager().bindTexture(modTabs);
			if(modtabs1 == null){
				System.out.println("modtabs is null");
				continue;
			}
			if(modtabs1.getTabIndex() != selectedTabIndex){
				this.func1(modtabs1);
			}
		}
		if(tabPage != 0){
			this.mc.getTextureManager().bindTexture(modTabs);
		}
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation("fireplacecore:textures/gui/" + modtabs.getBackgroundImageName()));
		this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		k = this.guiTop + 18;
		this.mc.getTextureManager().bindTexture(modTabs);
		if (modtabs.getTabPage() != tabPage)
		{
			return;
		}
		this.func1(modtabs);
	}
	protected boolean switchToTab(ModTabs tab, int int1, int int2){
		if(tab.getTabPage() != tabPage){
			return false;
		}
		int k = tab.getTabColumn();
		int l = 28 * k;
		byte b0 = 0;

		if (k == 5)
		{
			l = this.xSize - 28 + 2;
		}
		else if (k > 0)
		{
			l += k;
		}

		int i1;
		if(tab.isTabInFirstRow()){
			i1 = b0 - 32;
		}else{
			i1 = b0 + this.ySize;
		}
		return int1 >= l && int1 <= l + 28 && int2 >= i1 && int2 <= i1 + 32;
	}
	protected boolean renderModInventoryHoveringText(ModTabs tab, int int1, int int2){
		int k = tab.getTabColumn();
		int l = 28 * k;
		byte b0 = 0;

		if (k == 5)
		{
			l = this.xSize - 28 + 2;
		}
		else if (k > 0)
		{
			l += k;
		}

		int i1;

		if (tab.isTabInFirstRow())
		{
			i1 = b0 - 32;
		}
		else
		{
			i1 = b0 + this.ySize;
		}

		if (this.isPointInRegion(l + 3, i1 + 3, 23, 27, int1, int2))
		{
			this.drawCreativeTabHoveringText(I18n.format(tab.getTranslatedTabLabel(), new Object[0]), int1, int2);
			return true;
		}
		else
		{
			return false;
		}
	}
	protected void func1(ModTabs tab){
		boolean flag = tab.getTabIndex() == selectedTabIndex;
		boolean flag1 = tab.isTabInFirstRow();
		int i = tab.getTabColumn();
		int j = i * 28;
		int k = 0;
		int l = this.guiLeft + 28 * i;
		int i1 = this.guiTop;
		byte b0 = 32;

		if (flag)
		{
			k += 32;
		}

		if (i == 5)
		{
			l = this.guiLeft + this.xSize - 28;
		}
		else if (i > 0)
		{
			l += i;
		}

		if (flag1)
		{
			i1 -= 28;
		}
		else
		{
			k += 64;
			i1 += this.ySize - 4;
		}

		GlStateManager.disableLighting();
		GlStateManager.color(1F, 1F, 1F); //Forge: Reset color in case Items change it.
		GlStateManager.enableBlend(); //Forge: Make sure blend is enabled else tabs show a white border.
		this.drawTexturedModalRect(l, i1, j, k, 28, b0);
		this.zLevel = 100.0F;
		this.itemRender.zLevel = 100.0F;
		l += 6;
		i1 += 8 + (flag1 ? 1 : -1);
		GlStateManager.enableLighting();
		GlStateManager.enableRescaleNormal();
		ItemStack itemstack = tab.getIconItemStack();
		this.itemRender.renderItemAndEffectIntoGUI(itemstack, l, i1);
		this.itemRender.renderItemOverlays(this.fontRendererObj, itemstack, l, i1);
		GlStateManager.disableLighting();
		this.itemRender.zLevel = 0.0F;
		this.zLevel = 0.0F;
	}
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if (button.id == 101)
		{
			tabPage = Math.max(tabPage - 1, 0);
		}
		else if (button.id == 102)
		{
			tabPage = Math.min(tabPage + 1, maxPages);
		}
	}
	public int getSelectedTabIndex()
	{
		return selectedTabIndex;
	}
	protected boolean isPointInRegion(int left, int top, int right, int bottom, int pointX, int pointY)
	{
		int k1 = this.guiLeft;
		int l1 = this.guiTop;
		pointX -= k1;
		pointY -= l1;
		return pointX >= left - 1 && pointX < left + right + 1 && pointY >= top - 1 && pointY < top + bottom + 1;
	}
}
