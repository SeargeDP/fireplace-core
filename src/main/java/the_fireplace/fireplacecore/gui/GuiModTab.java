package the_fireplace.fireplacecore.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
/**
 * Make your gui you want rendered in your ModTab extend this
 * @author The_Fireplace
 *
 */
public class GuiModTab extends GuiScreen {
	protected int guiLeft, guiTop, xSize = 195, ySize = 136;
	public GuiModTab(){
		super();
		this.mc = Minecraft.getMinecraft();
		ScaledResolution res = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
		this.allowUserInput = true;
		this.width = res.getScaledWidth();
		this.height = res.getScaledHeight();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
	}
	@Override
	public void initGui(){
		super.initGui();
		ScaledResolution res = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
		this.allowUserInput = true;
		this.width = res.getScaledWidth();
		this.height = res.getScaledHeight();
		this.guiLeft = (this.width - this.xSize) / 2;
		this.guiTop = (this.height - this.ySize) / 2;
	}
}
