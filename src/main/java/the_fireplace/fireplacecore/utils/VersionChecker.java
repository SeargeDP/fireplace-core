package the_fireplace.fireplacecore.utils;

import java.util.ArrayList;
import java.util.Map;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.Loader;
import the_fireplace.fireplacecore.FireCoreBaseFile;
import the_fireplace.fireplacecore.logger.Logger;
import the_fireplace.fireplacecore.math.VersionMath;

import com.google.common.collect.Maps;
/**
 *
 * @author The_Fireplace
 *
 */
public class VersionChecker {
	private ArrayList modids;
	private final Map modNames = Maps.newHashMap();
	private final Map currVers = Maps.newHashMap();
	private final Map betaVers = Maps.newHashMap();
	private final Map relVers = Maps.newHashMap();
	private final Map mURLs = Maps.newHashMap();

	public VersionChecker(){
		modids = new ArrayList();
	}

	public void addMod(String modid, String modDisplayName, String currentVersion, String newBetaVersion, String newFullVersion, String updateURL){
		Logger.addToLog(FireCoreBaseFile.MODID, "Attempting to register "+modDisplayName+"("+modid+") to the version checker.");
		if(!modids.contains(modid)){
			modids.add(modid);
			Logger.addToLog(FireCoreBaseFile.MODID, modDisplayName+"("+modid+") was successfully registered.");
		}else{
			Logger.addToLog(FireCoreBaseFile.MODID, "Error: "+modid+" has already been registered.");
			return;
		}
		modNames.put(modid, modDisplayName);
		currVers.put(modid, currentVersion);
		betaVers.put(modid, newBetaVersion);
		relVers.put(modid, newFullVersion);
		mURLs.put(modid, updateURL);
	}

	public void onServerStarted(){
		for(int i=0;i<modids.size();i++){
			notifyServer((String)modids.get(i));
		}
	}

	private void notifyServer(String modid) {
		String VERSION = (String) currVers.get(modid);
		String MODNAME = (String) modNames.get(modid);
		String RELVER = (String) relVers.get(modid);
		String BETAVER = (String) betaVers.get(modid);
		String downloadURL = (String) mURLs.get(modid);
		switch (FireCoreBaseFile.instance.getUpdateNotification()) {
		case 0:
			if (VersionMath.isHigherVersion(VERSION, RELVER) && VersionMath.isHigherVersion(BETAVER, RELVER)) {
				notifyServer(MODNAME, RELVER, downloadURL);
			}else if(VersionMath.isHigherVersion(VERSION, BETAVER)){
				notifyServer(MODNAME, BETAVER, downloadURL);
			}

			break;
		case 1:
			if (VersionMath.isHigherVersion(VERSION, RELVER)) {
				notifyServer(MODNAME, RELVER, downloadURL);
			}
			break;
		case 2:
			break;
		}
	}

	private void notifyServer(String modname, String relver, String downloadURL) {
		Logger.addToLog(FireCoreBaseFile.MODID, "Notified server of the available update to "+relver);
		System.out.println(notificationString(modname,relver,downloadURL));
	}

	public void onPlayerJoinClient(EntityPlayer player){
		for(int i=0;i<modids.size();i++){
			notifyClient(player, (String) modids.get(i));
		}
	}

	private String notificationString(String modname, String version, String url){
		return "A new version of "+modname+" is available!\n=========="
				+ version
				+ "==========\n"
				+ "Download it at "+url+" !";
	}

	public void notifyClient(EntityPlayer player, String modid){
		String VERSION = (String) currVers.get(modid);
		String MODNAME = (String) modNames.get(modid);
		String RELVER = (String) relVers.get(modid);
		String BETAVER = (String) betaVers.get(modid);
		String downloadURL = (String) mURLs.get(modid);
		switch (FireCoreBaseFile.instance.getUpdateNotification()) {
		case 0:
			if (VersionMath.isHigherVersion(VERSION, RELVER) && VersionMath.isHigherVersion(BETAVER, RELVER)) {
				notifyClient(player, MODNAME, RELVER, downloadURL);
			}else if(VersionMath.isHigherVersion(VERSION, BETAVER)){
				notifyClient(player, MODNAME, BETAVER, downloadURL);
			}

			break;
		case 1:
			if (VersionMath.isHigherVersion(VERSION, RELVER)) {
				notifyClient(player, MODNAME, RELVER, downloadURL);
			}
			break;
		case 2:
			break;
		}
	}

	private void notifyClient(EntityPlayer player, String modname,
			String version, String downloadURL) {
		if(!Loader.isModLoaded("VersionChecker")){
			Logger.addToLog(FireCoreBaseFile.MODID, "Notified user of the available update to "+version);
			ChatUtils.putMessageInChat(
					player,
					notificationString(modname,version,downloadURL));
		}
	}
}
