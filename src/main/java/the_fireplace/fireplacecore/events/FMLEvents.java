package the_fireplace.fireplacecore.events;

import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import the_fireplace.fireplacecore.FireCoreBaseFile;
/**
 *
 * @author The_Fireplace
 *
 */
public class FMLEvents {
	@SubscribeEvent
	public void onPlayerJoinClient(final ClientConnectedToServerEvent event) {
		(new Thread() {
			@Override
			public void run() {
				while (FMLClientHandler.instance().getClientPlayerEntity() == null)
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}

				FireCoreBaseFile.onPlayerJoinClient(FMLClientHandler.instance()
						.getClientPlayerEntity());
				FireCoreBaseFile.instance.vc.onPlayerJoinClient(FMLClientHandler.instance()
						.getClientPlayerEntity());
			}
		}).start();
	}

	@SubscribeEvent
	public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
		if(eventArgs.modID.equals(FireCoreBaseFile.MODID))
			FireCoreBaseFile.instance.syncConfig();
	}
}
