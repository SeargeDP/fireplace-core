package the_fireplace.fireplacecore.compat.oldjava;

import java.time.LocalDateTime;

public class NewJava implements IOldCompat {

	@Override
	public String getDate() {
		return LocalDateTime.now().toString();
	}
}
