package the_fireplace.fireplacecore.config;


/**
 *
 * @author The_Fireplace
 *
 */
public class FCCV {
	public static final String UPDATETYPE_DEFAULT = "all";
	public static String UPDATETYPE;
	public static final String UPDATETYPE_NAME = "UpdateType";
	public static final boolean VCSWITCH_DEFAULT = true;
	public static boolean VCSWITCH;
	public static final String VCSWITCH_NAME = "VersionCheckerSwitch";
	/**
	 * @deprecated as of 2.2.3.0. Remove all developer codes.
	 */
	@Deprecated
	public static boolean hasCode(String code){
		return false;
	}
}
