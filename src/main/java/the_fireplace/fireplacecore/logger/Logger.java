package the_fireplace.fireplacecore.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import the_fireplace.fireplacecore.compat.oldjava.IOldCompat;
import the_fireplace.fireplacecore.compat.oldjava.NewJava;
import the_fireplace.fireplacecore.compat.oldjava.OldJava;
import the_fireplace.fireplacecore.math.Tools;
/**
 *
 * @author The_Fireplace
 *
 */
public class Logger {
	public static void addToLog(String modid, String texttoadd){
		BufferedWriter out = null;
		IOldCompat compat;
		if(Tools.getJavaVersion() >= 1.8){
			compat = new NewJava();
		}else{
			compat = new OldJava();
		}
		String time = compat.getDate();
		try {
			out = new BufferedWriter(new FileWriter(modid+".log", true));
			out.write(time+": "+texttoadd+"\n");
		} catch (IOException e) {

		} finally {
			if(out != null) {
				try {
					out.close();
				} catch (IOException e) {
					System.out.println("SEVERE ERROR: Fireplace Core Logger's BufferedWriter failed to close. Continual use of logging may result in a memory leak. Contact The_Fireplace immediately.");
				}
			}
		}
	}
	public static void create(String modid){
		String intro = "Fireplace Core Logger v1.1\n";
		PrintWriter writer;
		IOldCompat compat;
		if(Tools.getJavaVersion() >= 1.8){
			compat = new NewJava();
		}else{
			compat = new OldJava();
		}
		String time = compat.getDate();
		File file = new File(modid+".log");
		if(!file.exists()){
			try {
				writer = new PrintWriter(modid+".log", "UTF-8");
				writer.println(intro+"Log for "+modid+" created at "+time);
				writer.close();
			} catch (FileNotFoundException e) {
			} catch (UnsupportedEncodingException e) {
			}
		}else{
			if(file.delete()){
				try {
					writer = new PrintWriter(modid+".log", "UTF-8");
					writer.println(intro+"Log for "+modid+" cleared at "+time);
					writer.close();
				} catch (FileNotFoundException e) {
				} catch (UnsupportedEncodingException e) {
				}
			}else{
				System.out.println(intro+"Log for "+modid+" failed to clear.");
			}
		}
	}
}
